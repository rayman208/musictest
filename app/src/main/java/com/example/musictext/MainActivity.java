package com.example.musictext;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button buttonDo, buttonRe, buttonMi, buttonFa, buttonSol, buttonLya, buttonSi;

    private SoundPool
            soundPoolDo,
            soundPoolRe,
            soundPoolMi,
            soundPoolFa,
            soundPoolSol,
            soundPoolLya,
            soundPoolSi;

    private int Do, Re, Mi, Fa, Sol, Lya, Si;

    private TextView textViewNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonDo = findViewById(R.id.buttonDo);
        buttonRe = findViewById(R.id.buttonRe);
        buttonMi = findViewById(R.id.buttonMi);
        buttonFa = findViewById(R.id.buttonFa);
        buttonSol = findViewById(R.id.buttonSol);
        buttonLya = findViewById(R.id.buttonLya);
        buttonSi = findViewById(R.id.buttonSi);

        textViewNote = findViewById(R.id.textViewNote);

        AudioAttributes audioAttributes = new AudioAttributes.Builder().
                setUsage(AudioAttributes.USAGE_GAME).
                setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).
                build();

        soundPoolDo = new SoundPool.Builder().
                setAudioAttributes(audioAttributes).
                build();

        soundPoolRe = new SoundPool.Builder().
                setAudioAttributes(audioAttributes).
                build();

        soundPoolMi = new SoundPool.Builder().
                setAudioAttributes(audioAttributes).
                build();

        soundPoolFa = new SoundPool.Builder().
                setAudioAttributes(audioAttributes).
                build();

        soundPoolSol = new SoundPool.Builder().
                setAudioAttributes(audioAttributes).
                build();

        soundPoolLya = new SoundPool.Builder().
                setAudioAttributes(audioAttributes).
                build();

        soundPoolSi = new SoundPool.Builder().
                setAudioAttributes(audioAttributes).
                build();

        Do = soundPoolDo.load(this, R.raw.ndo,1);
        Re = soundPoolRe.load(this, R.raw.re,1);
        Mi = soundPoolMi.load(this, R.raw.mi,1);
        Fa = soundPoolFa.load(this, R.raw.fa,1);
        Sol = soundPoolSol.load(this, R.raw.sol,1);
        Lya = soundPoolLya.load(this, R.raw.lja,1);
        Si = soundPoolSi.load(this, R.raw.si,1);

        buttonDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPoolDo.play(Do,1,1,0,0,1);
                textViewNote.setText(textViewNote.getText().toString()+"-Do");
                textViewNote.setBackgroundColor(getResources().getColor(R.color.red));
            }
        });

        buttonRe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPoolRe.play(Re,1,1,0,0,1);
                textViewNote.setText(textViewNote.getText().toString()+"-Re");
                textViewNote.setBackgroundColor(getResources().getColor(R.color.orange));
            }
        });

        buttonMi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPoolMi.play(Mi,1,1,0,0,1);
                textViewNote.setText(textViewNote.getText().toString()+"-Mi");
                textViewNote.setBackgroundColor(getResources().getColor(R.color.yellow));
            }
        });

        buttonFa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPoolFa.play(Fa,1,1,0,0,1);
                textViewNote.setText(textViewNote.getText().toString()+"-Fa");
                textViewNote.setBackgroundColor(getResources().getColor(R.color.green));
            }
        });

        buttonSol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPoolSol.play(Sol,1,1,0,0,1);
                textViewNote.setText(textViewNote.getText().toString()+"-Sol");
                textViewNote.setBackgroundColor(getResources().getColor(R.color.skyblue));
            }
        });

        buttonLya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPoolLya.play(Lya,1,1,0,0,1);
                textViewNote.setText(textViewNote.getText().toString()+"-Lya");
                textViewNote.setBackgroundColor(getResources().getColor(R.color.blue));
            }
        });

        buttonSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundPoolSi.play(Si,1,1,0,0,1);
                textViewNote.setText(textViewNote.getText().toString()+"-Si");
                textViewNote.setBackgroundColor(getResources().getColor(R.color.violet));
            }
        });
    }
}